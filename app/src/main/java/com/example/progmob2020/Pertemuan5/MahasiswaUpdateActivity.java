package com.example.progmob2020.Pertemuan5;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MahasiswaUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mahasiswa_update);
        EditText edNimCari = (EditText) findViewById(R.id.tvNik);
        EditText edNama = (EditText) findViewById(R.id.tvNama);
        EditText edNimBaru = (EditText) findViewById(R.id.tvNikBaru);
        EditText edAlamat = (EditText) findViewById(R.id.tvAlamat);
        EditText edEmail = (EditText) findViewById(R.id.tvEmail);
        Button btnSimpan = (Button) findViewById(R.id.buttonSimpanDosen1);
        pd = new ProgressDialog(MahasiswaUpdateActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Tunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> del = service.delete_mhs(edNimCari.getText().toString(), "72180247");
//                    Call<DefaultResult> call = service.update_mhs(
//                            edNimCari.getText().toString(),
//                            edNama.getText().toString(),
//                            edNimBaru.getText().toString(),
//                            edAlamat.getText().toString(),
//                            edEmail.getText().toString(),
//                            "72180247"
//                    );

                del.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
//                            pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Data Berhasil Diubah", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this, "Gagal gan", Toast.LENGTH_LONG).show();
                    }
                });
                Call<DefaultResult> add= service.add_mhs(edNama.getText().toString(),
                        edNimBaru.getText().toString(), edAlamat.getText().toString(),
                        edEmail.getText().toString(),"Tidak perlu diisi","72180247"
                );
                add.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this,"berhasil",Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MahasiswaUpdateActivity.this,"Error",Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

    }
}

