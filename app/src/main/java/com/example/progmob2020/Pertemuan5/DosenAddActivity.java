package com.example.progmob2020.Pertemuan5;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DosenAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_add);

        final EditText tbNik = (EditText)findViewById(R.id.tvNik);
        final EditText tbNama = (EditText)findViewById(R.id.tvNama);
        final EditText tbAlamat = (EditText)findViewById(R.id.tvAlamat);
        final EditText tbEmail = (EditText)findViewById(R.id.tvEmail);
        Button btnSimpan = (Button)findViewById(R.id.buttonSimpanMatkul);
        pd = new ProgressDialog(DosenAddActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dosen(
                        tbNama.getText().toString(),
                        tbNik.getText().toString(),
                        tbAlamat.getText().toString(),
                        tbEmail.getText().toString(),
                        "Kosongkan saja",
                        "72180247"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(DosenAddActivity.this, "DATA GAGAL DISIMPAN", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}