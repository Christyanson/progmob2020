package com.example.progmob2020.Pertemuan5;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulAddActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_add);
        final EditText tvHari = (EditText) findViewById(R.id.tvEmail);
        final EditText tvKode = (EditText) findViewById(R.id.tvNama);
        final EditText tvNama = (EditText) findViewById(R.id.tvAlamat);
        final EditText tvSesi = (EditText) findViewById(R.id.tvSesi);
        final EditText tvSks = (EditText) findViewById(R.id.tvSks);
        Button btnSimpan = (Button) findViewById(R.id.buttonSimpanMatkul);
        pd = new ProgressDialog(MatkulAddActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon Menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matkul(
                        tvKode.getText().toString(),
                        tvNama.getText().toString(),
                        tvHari.getText().toString(),
                        tvSks.getText().toString(),
                        tvSesi.getText().toString(),
                        "72180247"
                );

                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulAddActivity.this, "DATA GAGAL DISIMPAN", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}
