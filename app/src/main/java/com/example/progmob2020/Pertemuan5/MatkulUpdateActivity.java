package com.example.progmob2020.Pertemuan5;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatkulUpdateActivity extends AppCompatActivity {
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matkul_update);

        final EditText upCari = (EditText)findViewById(R.id.tvKode);
        final EditText upNama = (EditText)findViewById(R.id.tvNama);
        final EditText upKode = (EditText)findViewById(R.id.tvKodeBaru);
        final EditText upHari = (EditText)findViewById(R.id.tvHari);
        final EditText upSesi = (EditText)findViewById(R.id.tvSesi);
        final EditText upSks = (EditText)findViewById(R.id.tvSks);
        Button btnUpdate = (Button) findViewById(R.id.btnUpdate);
        pd = new ProgressDialog(MatkulUpdateActivity.this);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd.setTitle("Mohon bersabar");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> del= service.delete_matkul(upCari.getText().toString(), "72180247"
                );
                del.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Toast.makeText(MatkulUpdateActivity.this,"Berhasil Update", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this,"Error Ganti!",Toast.LENGTH_LONG).show();
                    }
                });
                Call<DefaultResult> add= service.add_matkul(
                        upNama.getText().toString(),
                        upKode.getText().toString(),
                        upHari.getText().toString(),
                        upSesi.getText().toString(),
                        upSks.getText().toString(),
                        "72180247"
                );
                add.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this,"berhasil",Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(MatkulUpdateActivity.this,"Error",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}