package com.example.progmob2020.Main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.progmob2020.CRUD.DosenGetAllActivity;
import com.example.progmob2020.Pertemuan5.DosenAddActivity;
import com.example.progmob2020.Pertemuan5.DosenDeleteActivity;
import com.example.progmob2020.Pertemuan5.DosenUpdateActivity;
import com.example.progmob2020.R;

public class MainDosenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_dosen);

        //Button
        Button btnGetDosen =(Button)findViewById(R.id.buttonGetMatkul);
        Button btnAddDosen = (Button)findViewById(R.id.buttonAddMatkul);
        Button btnHapus = (Button)findViewById(R.id.buttonHapus);
        Button btnUpdate = (Button)findViewById(R.id.buttonUpdate);

        //Action
        btnGetDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenGetAllActivity.class);
                startActivity(intent);
            }
        });
        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenDeleteActivity.class);
                startActivity(intent);
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenUpdateActivity.class);
                startActivity(intent);
            }
        });

        btnAddDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainDosenActivity.this, DosenAddActivity.class);
                startActivity(intent);
            }
        });
    }

}