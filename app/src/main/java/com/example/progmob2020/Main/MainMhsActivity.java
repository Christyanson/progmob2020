package com.example.progmob2020.Main;

import androidx.appcompat.app.AppCompatActivity;
import com.example.progmob2020.CRUD.MahasiswaGetAllActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.progmob2020.Pertemuan5.MahasiswaAddActivity;
import com.example.progmob2020.Pertemuan5.MahasiswaDeleteActivity;
import com.example.progmob2020.Pertemuan5.MahasiswaUpdateActivity;
import com.example.progmob2020.R;

public class MainMhsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mhs);

        Button btnGet = (Button)findViewById(R.id.buttonGetMhs);
        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MahasiswaGetAllActivity.class);
                startActivity(intent);
            }
        });

        Button btnAddMhs = (Button)findViewById(R.id.buttonAddMhs);
        btnAddMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MahasiswaAddActivity.class);
                startActivity(intent);
            }
        });

        Button btnUpdateMhs = (Button)findViewById(R.id.buttonUpdate);
        btnUpdateMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MahasiswaUpdateActivity.class);
                startActivity(intent);
            }
        });

        Button btnHapus = (Button)findViewById(R.id.buttonHapus);
        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMhsActivity.this, MahasiswaDeleteActivity.class);
                startActivity(intent);
            }
        });
    }

}