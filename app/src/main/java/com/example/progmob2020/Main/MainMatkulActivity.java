package com.example.progmob2020.Main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.progmob2020.CRUD.MatkulGetAllActivity;
import com.example.progmob2020.Pertemuan5.MatkulAddActivity;
import com.example.progmob2020.Pertemuan5.MatkulDeleteActivity;
import com.example.progmob2020.Pertemuan5.MatkulUpdateActivity;
import com.example.progmob2020.R;

public class MainMatkulActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_matkul);

        //Button
        Button btnGetMatkul =(Button)findViewById(R.id.buttonGetMatkul);
        Button btnAddMatkul = (Button)findViewById(R.id.buttonAddMatkul);
        Button btnHapus = (Button)findViewById(R.id.buttonHapus);
        Button btnUpdate = (Button)findViewById(R.id.buttonUpdate);

        //Action
        btnGetMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatkulActivity.this, MatkulGetAllActivity.class);
                startActivity(intent);
            }
        });
        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatkulActivity.this, MatkulDeleteActivity.class);
                startActivity(intent);
            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatkulActivity.this, MatkulUpdateActivity.class);
                startActivity(intent);
            }
        });

        btnAddMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMatkulActivity.this, MatkulAddActivity.class);
                startActivity(intent);
            }
        });
    }

}