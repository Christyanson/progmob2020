package com.example.progmob2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020.Model.Dosen;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

public class DosenRecyclerAdapter extends RecyclerView.Adapter<DosenRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Dosen> dosenList;

    public DosenRecyclerAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }


    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list_cardview_dosen,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Dosen m = dosenList.get(position);
        holder.txtNama.setText(m.getNama());
        holder.txtNoTelp.setText(m.getNotelp());
        holder.txtNik.setText(m.getNik());
    }


    @Override
    public int getItemCount() {
        return dosenList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtNama, txtNik, txtNoTelp, tvAlamat, tvEmail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNama = itemView.findViewById(R.id.tvNik);
            txtNik  = itemView.findViewById(R.id.tvNama);
            txtNoTelp = itemView.findViewById(R.id.tvAlamat);
            tvAlamat = itemView.findViewById(R.id.tvAlamat);
            tvEmail = itemView.findViewById(R.id.tvSesi);
        }
    }
}
