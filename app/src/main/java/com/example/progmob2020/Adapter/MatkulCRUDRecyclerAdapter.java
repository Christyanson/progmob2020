package com.example.progmob2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020.Model.Matkul;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

public class MatkulCRUDRecyclerAdapter extends RecyclerView.Adapter<MatkulCRUDRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Matkul> matkulList;

    public MatkulCRUDRecyclerAdapter(Context context) {
        this.context = context;
        matkulList = new ArrayList<>();
    }

    public MatkulCRUDRecyclerAdapter(List<Matkul> matkulList) {
        this.matkulList = matkulList;
    }

    public List<Matkul> getMatkulList() {
        return matkulList;
    }

    public void setMatkulList(List<Matkul> matkulList) {
        this.matkulList = matkulList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MatkulCRUDRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_matkul, parent, false);
        return new MatkulCRUDRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MatkulCRUDRecyclerAdapter.ViewHolder holder, int position) {
        Matkul m = matkulList.get(position);

        holder.txtNama.setText(m.getNama());
        holder.txtKode.setText(m.getKode());
        holder.txtHari.setText(m.getHari());
        holder.txtSesi.setText(m.getSesi());
        holder.txtSks.setText(m.getSks());
    }

    @Override
    public int getItemCount() {
        return matkulList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtNama, txtKode, txtHari, txtSesi, txtSks;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtKode = itemView.findViewById(R.id.txtKode);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtHari = itemView.findViewById(R.id.txtHari);
            txtSesi = itemView.findViewById(R.id.txtSesi);
            txtSks = itemView.findViewById(R.id.txtSks);
        }
    }
}