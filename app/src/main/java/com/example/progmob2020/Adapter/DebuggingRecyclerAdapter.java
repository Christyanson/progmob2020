package com.example.progmob2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import com.example.progmob2020.Model.MahasiswaDebugging;
import com.example.progmob2020.R;


public class DebuggingRecyclerAdapter extends RecyclerView.Adapter<DebuggingRecyclerAdapter.ViewHolder> {

    private Context context;
    private List<MahasiswaDebugging> mahasiswaDebugging;

    public DebuggingRecyclerAdapter(Context context) {
        this.context = context;
        mahasiswaDebugging = new ArrayList<>();
    }

    public List<MahasiswaDebugging> getMahasiswaDebugging() {
        return mahasiswaDebugging;
    }

    public void setMahasiswaDebugging(List<MahasiswaDebugging> mahasiswaDebugging) {
        this.mahasiswaDebugging = mahasiswaDebugging;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_list_cardview,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MahasiswaDebugging m = mahasiswaDebugging.get(position);

        holder.tvNama.setText(m.getNama());
        holder.tvNoTelp.setText(m.getNotelp());
        holder.tvNim.setText(m.getNim());
    }

    @Override
    public int getItemCount() {
        return mahasiswaDebugging.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tvNama, tvNim, tvNoTelp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNama = itemView.findViewById(R.id.tvNik);
            tvNim = itemView.findViewById(R.id.tvNama);
            tvNoTelp = itemView.findViewById(R.id.tvAlamat);
        }
    }
}
