package com.example.progmob2020.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.progmob2020.Model.Dosen;
import com.example.progmob2020.R;

import java.util.ArrayList;
import java.util.List;

public class DosenCRUDRecyclerAdapter extends RecyclerView.Adapter<DosenCRUDRecyclerAdapter.ViewHolder> {
    Context context;
    List<Dosen> dosenList;

    public DosenCRUDRecyclerAdapter(Context context) {
        this.context = context;
        dosenList = new ArrayList<>();
    }

    public DosenCRUDRecyclerAdapter(List<Dosen> dosenList) {

        this.dosenList = dosenList;
    }

    public List<Dosen> getDosenList() {
        return dosenList;
    }

    public void setDosenList(List<Dosen> dosenList) {
        this.dosenList = dosenList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_cardview_dosen, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Dosen m = dosenList.get(position);
        holder.textViewNama.setText(m.getNama());
        holder.textViewNik.setText(m.getNik());
        //holder.txtNoTelp.setText(m.getNoTelp());
        holder.textViewAlamat.setText(m.getAlamat());
        holder.textViewEmail.setText(m.getEmail());
    }

    @Override
    public int getItemCount() {
        return dosenList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textViewNama, textViewNik, txtNoTelp, textViewAlamat, textViewEmail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewNama = itemView.findViewById(R.id.textViewNama);
            textViewNik  = itemView.findViewById(R.id.textViewNik);
            //txtNoTelp = itemView.findViewById(R.id.txNoTelp);
            textViewAlamat  = itemView.findViewById(R.id.textViewAlamat);
            textViewEmail  = itemView.findViewById(R.id.textViewEmail);
        }
    }
}
