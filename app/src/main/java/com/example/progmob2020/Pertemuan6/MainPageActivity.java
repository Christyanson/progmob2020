package com.example.progmob2020.Pertemuan6;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.progmob2020.Main.MainDosenActivity;
import com.example.progmob2020.Main.MainMatkulActivity;
import com.example.progmob2020.Main.MainMhsActivity;
import com.example.progmob2020.R;

public class MainPageActivity extends AppCompatActivity {

    private Button btnLogout;
    TextView txtPengguna;
    String resultNama;
    private String KEY_NAME = "NAMA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);

        SharedPreferences pref = MainPageActivity.this.getSharedPreferences("prefs_file", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

//        androidx.appcompat.widget.Toolbar myToolbar = (Toolbar)findViewById(R.id.toolbar);
//        setSupportActionBar(myToolbar);
        //button
        Button btnMatkul = (Button)findViewById(R.id.btnMatkul);
        btnMatkul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainPageActivity.this, MainMatkulActivity.class));
                finish();
            }
        });

        Button btnDosen = (Button)findViewById(R.id.btnDosen);
        btnDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainPageActivity.this, MainDosenActivity.class));
                finish();
            }
        });

        Button btnMhs = (Button)findViewById(R.id.btnMhs);
        btnMhs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainPageActivity.this, MainMhsActivity.class));
                finish();
            }
        });

        //Munculin kata HAI dan nim dari login
//        txtHai = (TextView) findViewById(R.id.txtPengguna);
//        Bundle extras = getIntent().getExtras();
//        if (extras !=null)
//            resultNama = extras.getString("result_nama");
//            txtPengguna.setText(resultNama);
//        nama = extras.getString(KEY_NAME);

        Button btnLogout = (Button)findViewById(R.id.btnLogOut);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainPageActivity.this, SplashActivity.class);
                startActivity(intent);
            }
        });
    }

}