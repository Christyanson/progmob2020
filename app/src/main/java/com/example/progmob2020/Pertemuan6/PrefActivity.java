package com.example.progmob2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.progmob2020.Model.User;
import com.example.progmob2020.Network.GetDataService;
import com.example.progmob2020.Network.RetrofitClientInstance;
import com.example.progmob2020.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrefActivity extends AppCompatActivity {
    ProgressDialog pd;
    List<User> users;
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref2);
        Toast.makeText(PrefActivity.this, "", Toast.LENGTH_SHORT).show();
        session = PreferenceManager.getDefaultSharedPreferences(PrefActivity.this);
        if(!session.getString("nimnik", "").isEmpty() && !session.getString("nama", "").isEmpty()) {
            finish();
            startActivity(new Intent(PrefActivity.this, MainPageActivity.class));
            return;
        }

        EditText nimnik = (EditText) findViewById(R.id.edNim);
        EditText password = (EditText) findViewById(R.id.edPassword);
        Button btnSimpan = (Button)findViewById(R.id.btnPref3);
        pd = new ProgressDialog(PrefActivity.this);

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setTitle("Mohon menunggu");
                pd.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<User>> call = service.login(
                        nimnik.getText().toString(),
                        password.getText().toString()
                );

                call.enqueue(new Callback<List<User>>() {
                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                        pd.dismiss();
                        users = response.body();
                        assert users != null;
                        User u = users.get(0);
                        SharedPreferences.Editor editor = session.edit();
                        editor.clear();
                        editor.putString("nimnik", u.getNimnik());
                        editor.putString("nama", u.getNama());
                        editor.apply();
                        Intent intent = new Intent(PrefActivity.this, MainPageActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t) {
                        pd.dismiss();
                        Toast.makeText(PrefActivity.this, "Username atau password salah.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}