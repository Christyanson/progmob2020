package com.example.progmob2020.Network;

import com.example.progmob2020.Model.Dosen;
import com.example.progmob2020.Model.Mahasiswa;

import com.example.progmob2020.Model.DefaultResult;
import com.example.progmob2020.Model.Matkul;
import com.example.progmob2020.Model.User;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface GetDataService {
    @FormUrlEncoded
    @POST("api/progmob/login")
    Call<List<User>> login(
            @Field("nimnik") String nimnik,
            @Field("password") String password);

    @GET("api/progmob/mhs/{nim_progmob}")
    Call<List<Mahasiswa>> getMahasiswa(@Path("nim_progmob") String nim_progmob);

    @FormUrlEncoded
    @POST("api/progmob/mhs/delete")
    Call<DefaultResult>delete_mhs(
            @Field("nim") String id,
            @Field("nim_progmob") String nim_progmob
    );
    @FormUrlEncoded
    @POST("api/progmob/mhs/create")
    Call<DefaultResult> add_mhs(
            @Field("nama") String nama,
            @Field("nim") String nim,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );
    @FormUrlEncoded
    @POST("api/progmob/mhs/update")
    Call<DefaultResult>update_mhs(
            @Field("nama") String nama,
            @Field("nim") String nim,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );



    //Dosen
    @GET("api/progmob/dosen/{nim_progmob}")
    Call<List<Dosen>> getDosen(@Path("nim_progmob") String nim_progmob);

    @FormUrlEncoded
    @POST("api/progmob/dosen/delete")
    Call<DefaultResult>delete_dosen(
            @Field("id") String id,
            @Field("nim_progmob") String nim_progmob
    );
    @FormUrlEncoded
    @POST("api/progmob/dosen/create")
    Call<DefaultResult> add_dosen (
            @Field("nama") String nama,
            @Field("nik") String nik,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );
    @FormUrlEncoded
    @POST("api/progmob/dosen/update")
    Call<DefaultResult>update_dosen(
            @Field("nama") String nama,
            @Field("nik") String nik,
            @Field("alamat") String alamat,
            @Field("email") String email,
            @Field("foto") String foto,
            @Field("nim_progmob") String nim_progmob
    );

    //Matkul
    @GET("api/progmob/matkul/{nim_progmob}")
    Call<List<Matkul>> getMatkul(@Path("nim_progmob") String nim_progmob);

    @FormUrlEncoded
    @POST("api/progmob/matkul/delete")
    Call<DefaultResult>delete_matkul(
            @Field("id") String id,
            @Field("nim_progmob") String nim_progmob
    );

    @FormUrlEncoded
    @POST("api/progmob/matkul/create")
    Call<DefaultResult> add_matkul(
            @Field("nama") String nama,
            @Field("kode") String kode,
            @Field("hari") String hari,
            @Field("sesi") String sesi,
            @Field("sks") String sks,
            @Field("nim_progmob") String nim_progmob
    );
    @FormUrlEncoded
    @POST("api/progmob/matkul/update")
    Call<DefaultResult>update_matkul(
            @Field("nama") String nama,
            @Field("kode") String kode,
            @Field("hari") String hari,
            @Field("sesi") String sesi,
            @Field("sks") String sks,
            @Field("nim_progmob") String nim_progmob
    );
//
//    //Jadwal
//    @GET("api/progmob/jadwal/{nim_progmob}")
//    Call<List<Jadwal>> getJadwal(@Path("nik_progmob") String nik_progmob);
//
//    @FormUrlEncoded
//    @POST("api/progmob/dosen/delete")
//    Call<DefaultResult>delete_dosen(
//            @Field("nik") String id,
//            @Field("nik_progmob") String nim_progmob
//    );
//    @FormUrlEncoded
//    @POST("api/progmob/dosen/create")
//    Call<DefaultResult> add_dosen (
//            @Field("nama") String nama,
//            @Field("nik") String nik,
//            @Field("alamat") String alamat,
//            @Field("email") String email,
//            @Field("foto") String foto,
//            @Field("nik_progmob") String nik_progmob
//    );
//    @FormUrlEncoded
//    @POST("api/progmob/dosen/update")
//    Call<DefaultResult>update_dosen(
//            @Field("nama") String nama,
//            @Field("nik") String nik,
//            @Field("alamat") String alamat,
//            @Field("email") String email,
//            @Field("foto") String foto,
//            @Field("nik_progmob") String nik_progmob
//    );




//    @POST("api/progmob/mhs/login")
//    Call<AuthorizationResponse> login
//            (@Header("Authorization")String authorization);
    //Matkul
//    @GET("api/progmob/matkul/{nim_progmob}")
//    Call<List<Matkul>> getMatkul(@Path("nim_progmob") String nik_progmob);

}